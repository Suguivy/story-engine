#include "text.h"
#include <ncurses.h>

int current_pair = 0;

void load_colors() {
    init_pair(RED, COLOR_RED, COLOR_BLACK);
    init_pair(GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair(YELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair(BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(CYAN, COLOR_CYAN, COLOR_BLACK);
}

void iprint(const char* str) {
    const char *ptr = str;

    while(*ptr) {
	if (*ptr == '$') {
	    switch(*(++ptr)) {
	    case 'r':
		attron(COLOR_PAIR(current_pair = RED));
		break;
	    case 'g':
		attron(COLOR_PAIR(current_pair = GREEN));
		break;
	    case 'y':
		attron(COLOR_PAIR(current_pair = YELLOW));
		break;
	    case 'b':
		attron(COLOR_PAIR(current_pair = BLUE));
		break;
	    case 'm':
		attron(COLOR_PAIR(current_pair = MAGENTA));
		break;
	    case 'c':
		attron(COLOR_PAIR(current_pair = CYAN));
		break;
	    case '0':
		attroff(COLOR_PAIR(current_pair));
		break;
	    default:
		printw("%c", *ptr);
	    }
	} else {
	    printw("%c", *ptr);
	}
	ptr++;
    }
}

void reset_color() {
    attroff(COLOR_PAIR(current_pair));
}
