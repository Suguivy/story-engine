#include "scenes.h"
#include <lua5.1/lua.h>
#include <lua5.1/lualib.h>
#include <lua5.1/lauxlib.h>
#include <stdlib.h>

int main() {
    lua_State *L = luaL_newstate();
    
    if(load_scenes(L)) {
	return EXIT_FAILURE;
    }

    run_all_scenes(L);
    lua_close(L);

    return EXIT_SUCCESS;
    
}
