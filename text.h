#ifndef TEXT_H
#define TEXT_H

// Definitions for curses color pairs
#define RED 1
#define GREEN 2
#define YELLOW 3
#define BLUE 4
#define MAGENTA 5
#define CYAN 6

// Prints a string intepreting the format according to format.md, traducing it to its corresponding ANSI escape secuences
void iprint(const char* str);

// Loads curses color pairs
void load_colors();

// Reset text to current color
void reset_color();

#endif // INTERPRET_H
