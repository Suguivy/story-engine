# story-engine

An engine to write dialogues or stories on the terminal. This engine reads the `main.lua` file and makes a dialog with its contents.

## Usage

On the `main.lua` file there must be a Lua table called `scenes`, that contains more tables. Each table represents a scene that starts with the screen empty, and each string on it will be printed on the screen on separate lines. These tables can only contain strings.

There are a custom escape character, the dollar symbol (`$`). Some characters placed after `$` produce some efects, like text color. If the behavior of a character is not defined, the character is simply taken literally.

Note that the default escape character `\` has higher precedence and is evaluated before the `$`. For example, the string `"$\t"` will not be evaluated as `\t`, but as `HT` (a literal horizontal tab).

### Coloring text

To colorize text you must use the format `"$<color><text>"`, where `<color>` is the character representing a color (there are currently 7 colors, including the default color). The text after the sequence will be colorized according to the used character, unless there is another other color escape sequence or the end of a string be reached. If the latter occurs, the text color will be automatically changed to default for the next string.

The full color character list is the following:
- `0`: default.
- `r`: red.
- `g`: green.
- `y`: yellow.
- `b`: blue.
- `m`: magenta.
- `c`: cyan.

Example: `"roses are $rred"` -> `roses are red` (the word `red` will be in red color).
