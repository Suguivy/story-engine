#ifndef SCENES_H
#define SCENES_H

#include <lua5.1/lua.h>

#define MAIN_FILE "main.lua"

// Tries to load the main file and checks if there are errors in that file
int load_scenes(lua_State *L);

void run_all_scenes(lua_State *L);

void run_scene(lua_State *L);

#endif // SCENES_H
