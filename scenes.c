#include "scenes.h"
#include "text.h"
#include <lua5.1/lua.h>
#include <lua5.1/lualib.h>
#include <lua5.1/lauxlib.h>
#include <ncurses.h>
#include <stdlib.h>

int load_scenes(lua_State *L) {
    if(luaL_dofile(L, MAIN_FILE)) {
	printf("the main file doesn't exist");
	return -1;
    }

    lua_getglobal(L, "scenes");
    
    if(!lua_istable(L, -1)) {
	printf("scenes table not found");
	return -1;
    }
    
    return 0;
}

void run_all_scenes(lua_State *L) {
    initscr();
    noecho();
    start_color();
    load_colors();
    
    // Iterates over the scenes
    for(int i = 1; lua_pushnumber(L, i), lua_gettable(L, -2), !lua_isnil(L, -1); i++) {
	run_scene(L);
	erase();
	lua_pop(L, 1);
    }
    // Pops the last index pushed by the for loop (which value is nil)
    lua_pop(L, 1);

    endwin();
}

void run_scene(lua_State *L) {
    // Iterates over the strings in scene
    for(int i = 1; lua_pushnumber(L, i), lua_gettable(L, -2), !lua_isnil(L, -1); i++) {
	if(lua_isstring(L, -1)) {
	    iprint(lua_tostring(L, -1));
	    refresh();
	    reset_color();
	    getch();
	    printw("%c", '\n');
	}
	lua_pop(L, 1);
    }
    // Pops the last index pushed by the for loop (which value is nil)
    lua_pop(L, 1);
}
